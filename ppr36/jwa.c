#include <stdio.h>
#include <math.h>

float distanceBetweenPoints(float x1, float x2, float y1, float y2);
float countModule(float x, float y);

typedef struct Vertex
{
  float x;
  float y;
} Vertex;

int main()
{
  int numberOfVertices;
  float circumference = 0.0;
  float sumOfModules = 0.0;
  
  scanf("%d", &numberOfVertices);
  Vertex figure[numberOfVertices];
  
  for(int i=0; i<numberOfVertices; i++)
  {
    scanf("%f %f", &figure[i].x, &figure[i].y);
  }
  
  // count circumference
  for(int i=0; i<numberOfVertices-1; i++)
  {
    circumference += distanceBetweenPoints(figure[i].x, figure[i+1].x, figure[i].y, figure[i+1].y);
  }
  circumference += distanceBetweenPoints(figure[numberOfVertices-1].x, figure[0].x, figure[numberOfVertices-1].y, figure[0].y); // to add edge between last and first
  printf("%0.3f ", circumference);
  
  // count sum of modules
  for(int i=0; i<numberOfVertices; i++)
  {
    sumOfModules += countModule(figure[i].x, figure[i].y);
  }
  printf("%0.3f", sumOfModules);

  return 0;
}

float distanceBetweenPoints(float x1, float x2, float y1, float y2)
{
  return sqrt(pow(x1-x2, 2)+pow(y1-y2, 2));
}

float countModule(float x, float y)
{
  return sqrt(x*x+y*y);
}