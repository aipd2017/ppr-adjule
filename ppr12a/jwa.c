#include <stdio.h>

int main()
{
  int numberOfBoxes, questions;
  
  scanf("%d", &numberOfBoxes);
  int boxesArray[numberOfBoxes];
  
  for(int i=0; i<numberOfBoxes; i++)
  {
    scanf("%d", &boxesArray[i]);
  }
  
  scanf("%d", &questions);
  int first, second;
  int resultArray[questions];
  
  for(int i=0; i<questions; i++)
  {
    resultArray[i] = 0;
    scanf("%d %d", &first, &second);
    
    for(int j=first-1; j<second; j++)
    {
      resultArray[i] += boxesArray[j];
    }
  }
  
  for(int i=0; i<questions; i++)
  {
    printf("%d\n", resultArray[i]);
  }
  
  return 0;
}