date1 = list(map(int, input().split()))
date2 = list(map(int, input().split()))

if date1[0] < date2[0] or (date1[0] == date2[0] and date1[1] < date2[1]) or (date1[0] == date2[0] and date1[1] == date2[1] and date1[2] < date2[2]):
  print(date1[0], date1[1], date1[2], sep=" ")
else:
  print(date2[0], date2[1], date2[2], sep=" ")