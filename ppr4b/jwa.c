#include <stdio.h>

int main()
{
  int year1, month1, day1;
  int year2, month2, day2;
  
  scanf("%d %d %d", &year1, &month1, &day1);
  scanf("%d %d %d", &year2, &month2, &day2);
  
  if(year1 < year2 || year1 == year2 && month1 < month2
    || year1 == year2 && month1 == month2 && month1 < month2)
  {
    printf("%d %d %d", year1, month1, day1);
  }
  else
  {
    printf("%d %d %d", year2, month2, day2);
  }
  
  return 0;
}