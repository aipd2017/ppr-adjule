#include <stdio.h>

int main()
{
  int numberOfCalculations = 0;
  int firstNumber, secondNumber;
  char op;
  
  scanf("%d", &numberOfCalculations);
  
  for(int i=0; i<numberOfCalculations; i++)
  {
    scanf("%d %c %d", &firstNumber, &op, &secondNumber);
    
    if(op == '+')
      printf("%d\n", firstNumber+secondNumber);
    else if(op == '-')
      printf("%d\n", firstNumber-secondNumber);
    else if(op == '*')
      printf("%d\n", firstNumber*secondNumber);
  }
  return 0;
}