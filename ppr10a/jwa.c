#include <stdio.h>

int main()
{
  int numberOfTexts, numberOfChars;
  
  scanf("%d", &numberOfTexts);

  // load data about number of I, O and !
  for(int i=0; i<numberOfTexts; i++)
  {
    scanf("%d", &numberOfChars);
    
    // print BI..G B
    printf("B");
    for(int j=0; j<numberOfChars; j++)
    {
      printf("I");
    }
    printf("G B");
    
    // print O..M
    for(int j=0; j<numberOfChars; j++)
    {
      printf("O");
    }
    printf("M");
    
    // print !
    for(int j=0; j<numberOfChars; j++)
    {
      printf("!");
    }
    printf("\n");
  }
  
  return 0;
}