#include <stdio.h>

int main()
{
  int candidates, votes, vote, maxVotes=0, winner;
  
  scanf("%d %d", &candidates, &votes);
  int candidatesArray[candidates];
  
  // create empty table with zeros
  for(int i=0; i<candidates; i++)
  {
    candidatesArray[i] = 0;
  }
  
  // load votes to table
  for(int i=0; i<votes; i++)
  {
    scanf("%d3", &vote);
    candidatesArray[vote-1] += 1;
  }
  
  // print result
  for(int i=0; i<candidates; i++)
  {
    printf("%d: %d\n", i+1, candidatesArray[i]);
    if(candidatesArray[i] > maxVotes)
    {
      maxVotes = candidatesArray[i];
      winner = i+1;
    }
  }
  printf("%d", winner);
  
  return 0;
}