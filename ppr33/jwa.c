#include <stdio.h>

typedef struct Person
{
  char Name[20];
  char Surname[25];
  int Age;
  float Height;
} Person;

int main()
{
  int numberOfPeople, minAge;
  float minHeight;
  
  scanf("%d", &numberOfPeople);
  Person people[numberOfPeople];
  
  for(int i=0; i<numberOfPeople; i++)
  {
    scanf("%s %s %d %f", &people[i].Name, &people[i].Surname, &people[i].Age, &people[i].Height);
  }
  
  scanf("\n%d", &minAge);
  scanf("\n%f", &minHeight);
  
  for(int i=0; i<numberOfPeople; i++)
  {
    if(people[i].Age > minAge)
      printf("%s %s\n", people[i].Name, people[i].Surname);
  }
  printf("----\n");
  
  for(int i=0; i<numberOfPeople; i++)
  {
    if(people[i].Height < minHeight)
      printf("%s %s\n", people[i].Name, people[i].Surname);
  }

  return 0;
}