#include <stdio.h>

union StoredData
{
  int i;
  long l;
  float f;
  double d;
};

int main()
{
  union StoredData data;
  int numberOfQuestioners;
  char command[6];
  
  scanf("%f", &data.f);
  scanf("%d", &numberOfQuestioners);
  
  for(int i=0; i<numberOfQuestioners; i++)
  {
    scanf("%s", &command);
    
    if(command[0] == 'i')
    {
      printf("%d", data.i);  
    }
    else if(command[0] == 'l')
    {
      printf("%li", data.l);
    }
    else if(command[0] == 'f')
    {
      printf("%f", data.f);
    }
    else if(command[0] == 'd')
    {
      printf("%lf", data.d);
    }
  }

  return 0;
}