numberOfPeopleU, numberOfPeopleN, priceOfAllTickets = input().split()
result = float(priceOfAllTickets) / (float(numberOfPeopleN) + 0.5 * float(numberOfPeopleU))

print("{0:.2f}".format(result))