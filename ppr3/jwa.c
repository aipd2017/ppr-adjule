#include <stdio.h>

int main() {
  int u, n, s;
  float w;

  scanf("%d %d %d", &u, &n, &s);
  w = s / (n + 0.5 * u);
  printf("%0.2f", w);

  return 0;
}