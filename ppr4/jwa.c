#include <stdio.h>

int main()
{
  int start, end, playerShot;
  
  scanf("%d %d", &start, &end);
  scanf("%d", &playerShot);
  
  if(playerShot >= start && playerShot <= end)
  {
    printf("BINGO");
  }
  else
  {
    if(playerShot > end)
    {
      printf("%d", playerShot - end);
    }
    else
    {
      printf("%d", start - playerShot);
    }
  }
  
  return 0;
}