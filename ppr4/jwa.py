startRange, endRange = input().split()
shot = input()

startRange, endRange, shot = int(startRange), int(endRange), int(shot)

if shot < startRange:
  print(startRange - shot)
elif shot > endRange:
  print(shot - endRange)
else:
  print("BINGO")