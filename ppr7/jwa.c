#include <stdio.h>

int main()
{
  int n;
  scanf("%d", &n);
  
  int list[n];
  
  for(int i=0; i<n; i++)
  {
    int tmp;
    scanf("%d", &tmp);
    if(i%2 != 0 && i != 0)
      list[i] = tmp;
    else
      list[i] = -1;
  }
  
  for(int i=n-1; i>=0; i--)
  {
    if(list[i] != -1)
      printf("%d ", list[i]);
  }
  
  return 0;
}