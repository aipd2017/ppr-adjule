#include <stdio.h>

struct Friend
{
  char name[20];
  int education;
};

int GetEdLvl(char lvl);

int main()
{
  int numberOfFriends;
  char minEdLvl;
  
  scanf("%d", &numberOfFriends);
  struct Friend friendsTable[numberOfFriends];

  for(int i=0; i<numberOfFriends; i++)
  {
    char education;
    scanf("%s %c", &friendsTable[i].name, &education);
    friendsTable[i].education = GetEdLvl(education);
  }
  
  scanf("\n%c", &minEdLvl);
  
  for(int i=0; i<numberOfFriends; i++)
  {
    if(friendsTable[i].education >= GetEdLvl(minEdLvl))
      printf("%s\n", friendsTable[i].name);
  }
  
  return 0;
}

int GetEdLvl(char education)
{
  if(education == 'p')
    return 0;
  else if(education == 'g')
    return 1;
  else if(education == 's')
    return 2;
  else if(education == 'w')
    return 3;
}