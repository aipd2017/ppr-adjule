#include <stdio.h>

int main()
{
  int rounds, number1, number2, result;
  char option;
  
  scanf("%d", &rounds);
  
  for(int i=0; i<rounds; i++)
  {
    scanf("%d %d %c", &number1, &number2, &option);
    
    // swap if number1 > number2
    if(number1 > number2)
    {
      int tmp = number1;
      number1 = number2;
      number2 = tmp;
    }
    else if(number1 == number2)
      continue;
      
    // choose option
    result = (option == '*') ? 1 : 0;
    if(option == '+')
    {
      // count number of elements in sequence
      int n = number2 - number1 + 1;
      
      // use formula for the sum of n words of the arithmetic sequence
      if(option == '+')
        result = (number1 + number2) * n / 2;
    }
    else
    {
      for(int j=number1; j<=number2; j++)
      {
        if(option == '-')
          result -= j;
        else if(option == '*')
          result *= j;
      }
    }
    printf("%d", result);
  }
  
  return 0;
}