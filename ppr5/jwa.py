startRange, endRange = list(map(int, input().split()))
typeOfNumber = input()

for i in range(startRange, endRange + 1):
  if i%2 != 0 and typeOfNumber == 'n':
    print(i)
  elif i%2 == 0 and typeOfNumber == 'p':
    print(i)