#include <stdio.h>

int main()
{
  int start, end;
  char pN;
  
  scanf("%d %d", &start, &end);
  scanf(" %c", &pN);
  
  for(int i=start; i<=end; i++)
  {
    if(pN == 'p')
    {
      if(i%2 == 0) printf("%d ", i);
    }
    else if(pN == 'n')
    {
      if(i%2 != 0) printf("%d ", i);
    }
  }
  
  return 0;
}